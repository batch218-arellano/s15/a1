console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

// first name, last name, age - first 3  lines :

	let first = "John";
	console.log("First Name: " + first);

	let last = "Smith";
	console.log("Last Name: " + last);

	let ageJohn = "30";
	console.log("Age: " + ageJohn);

// hobbies: 
	
	let hobbyOne = "Hobbies: ";
	console.log(hobbyOne)
	let hobbyTwo = ["Biking", "Mountain Climbing", "Swimming"];
	console.log(hobbyTwo);

// Work Address: 
	
	let workOne = "Work Address: ";
	console.log(workOne)

	let workTwo = {
		houseNumber: "32",
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska",
	};

	console.log(workTwo);

// Steve Roggers' profile


	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony", "Bruce","Thor","Natasha","Clint", "Nick"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	};
	console.log("My Full Profile: ");
	console.log(profile); 

	let secName = "Bucky Barnes";
	console.log("My bestfriend is: " + secName);

	const lastLocation = ["Arctic Ocean"];
	lastLocation[0] = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

